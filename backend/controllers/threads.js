const dbCtrl = require('./db')


exports.getAllThreads = (req, res, next) => {
	let db ; try { db = dbCtrl.dbConnect() } catch { res.status(500).json({}) }
	db.all(
		"SELECT * FROM threads JOIN users ON threads.userId = users.userId ORDER BY id DESC",
		[],
		(err, row) => {
			if (err) 
				res.status(500).json({})
			else
				res.status(200).json(row)
		}
	)
	dbCtrl.dbClose(db)
}

const getProfile = (userId) => {
	return new Promise ((resolve, reject) => {
		let db ; try { db = dbCtrl.dbConnect() } catch { return null }
		db.get(
			"SELECT * FROM users WHERE userId=?",
			[userId],
			(err, row) => {
				dbCtrl.dbClose(db)
				if (row)
					resolve(row)
				else 
					reject(err)
			}
		)
	})
}

exports.createThread = async (req, res, next) => {
	let db ; try { db = dbCtrl.dbConnect() } catch(err) { return res.status(500).json({}) }
	let user = await getProfile(req.body.userId)
	let data = [req.body.userId, user.pseudo, req.body.title, req.body.text]
	db.run(
		'INSERT INTO threads (userId, username, title, text) VALUES ((?),(?),(?),(?))'
		, data
		, err => {
			if (err) {
				console.error(err.message ? err.message : err)
				res.status(500).json({})
			}
			else
				res.status(201).json({message: 'Discussion créée'})
		}
	)
	dbCtrl.dbClose(db)
}

exports.deleteThread =  (req, res, next) => {
	let db ; try { db = dbCtrl.dbConnect() } catch(err) { return res.status(500).json({}) }
	db.run('DELETE FROM threads WHERE id = (?)', [req.params.id] , err => {
		if (err)
			res.status(500).json({ error: 'Erreur pendant la suppression du sujet.' })
		else {
			db.run('DELETE FROM posts WHERE threads_id = (?)', [req.params.id] , err => {
				if (err)
					res.status(500).json({ error: 'Erreur pendant la suppression du sujet.' })
				else
					res.status(200).json({ message: 'Discussion supprimée.' })
			})
		}
	})
	dbCtrl.dbClose(db)
}
