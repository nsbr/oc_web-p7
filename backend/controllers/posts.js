const dbCtrl = require('./db')


exports.getAllPosts = (req, res, next) => {
	let db ; try { db = dbCtrl.dbConnect() } catch { res.status(500).json({}) }
	db.all(
		"SELECT * FROM posts JOIN users ON posts.userId = users.userId WHERE threads_id = (?) ORDER BY id ASC",
		[req.params.id],
		(err, row) => {
			if (err)
				res.status(500).json({})
			else
				res.status(200).json(row)
		}
	)
	dbCtrl.dbClose(db)
}

const getProfile = (userId) => {
	return new Promise ((resolve, reject) => {
		let db ; try { db = dbCtrl.dbConnect() } catch { return null }
		db.get(
			"SELECT * FROM users WHERE userId=?",
			[userId],
			(err, row) => {
				dbCtrl.dbClose(db)
				if (row)
					resolve(row)
				else 
					reject(err)
			}
		)
	})
}

exports.createPost = async (req, res, next) => {
	let db ; try { db = dbCtrl.dbConnect() } catch(err) { return res.status(500).json({}) }
	let user = await getProfile(req.body.userId)
	let data = [req.body.userId, user.pseudo, req.body.title, req.body.text, req.params.id]
	db.run(
		'INSERT INTO posts (userId, username, title, text, threads_id) VALUES ((?),(?),(?),(?),(?))'
		, data
		, err => {
			if (err) {
				console.error(err.message ? err.message : err)
				res.status(500).json({})
			}
			else
				res.status(201).json({message: 'Message enregistré'})
		}
	)
	dbCtrl.dbClose(db)
}

exports.deletePost =  (req, res, next) => {
	let db ; try { db = dbCtrl.dbConnect() } catch(err) { return res.status(500).json({}) }
	db.run('DELETE FROM posts WHERE id = (?)', [req.params.id] , err => {
		if (err)
			res.status(500).json({ error: 'Erreur pendant la suppression du sujet.' })
		else
			res.status(200).json({ message: 'Message supprimé.' })
	})
	dbCtrl.dbClose(db)
}
