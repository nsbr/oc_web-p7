const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const dbCtrl = require('./db')

const addUserDb = (data, res) => {
	userData = [
		data.email,
		data.pseudo,
		data.password,
		data.firstName,
		data.lastName,
		data.birthDate,
		data.country,
		data.city,
		data.job,
		data.hobbies
	]
	let db ; try { db = dbCtrl.dbConnect() } catch(err) { return res.status(500).json({}) }
	const placeholders = userData.map((data) => '(?)').join(',')
	db.run(
		`INSERT INTO users (email, pseudo, password, firstName, lastName, birthDate, country, city, job, hobbies) VALUES (${placeholders})`
		, userData
		, err => {
			if (err) {
				const constraintError = err.message.split(':').slice(-1)[0].trim()
				if (constraintError === "users.pseudo")
					res.status(400).json({ message: 'Le pseudo est déjà utilisé.' })
				else if (constraintError === "users.email")
					res.status(400).json({ message: 'L\'email est déjà utilisé.' })
				else
					res.status(500).json({})
			}
			else
				res.status(201).json({})
		}
	)
	dbCtrl.dbClose(db)
}

exports.signup = (req, res, next) => {
	bcrypt.hash(req.body.password, 10)
	.then (hash => {
		req.body.password = hash
		addUserDb(req.body, res)
	})
	.catch (err => res.status(500).json({ error: 'Server Error.'}))
}


exports.login = (req, res, next) => {
	let db ; try { db = dbCtrl.dbConnect() } catch { return res.status(500).json({}) }
	db.get(
		"SELECT email, password, userId FROM users WHERE email=?",
		[req.body.email],
		(err, row) => {
			if (err) {
				res.status(500).json({})
			}
			else if (row) {
				bcrypt.compare(req.body.password, row.password)
				.then (valid => {
					if (!valid)
						res.status(401).json( {message: 'Mauvaise combinaison d\'email et mot de passe.' })
					else
						res.status(200).json({
							userId: row.userId,
							token: jwt.sign(
								{ userId: row.userId},
								'WryubBedMyshoaDraBlabcod9drukJov',
								{ expiresIn: '24h' }
							)
						})
				})
				.catch(err => res.status(500).json({}))
			}
			else
				res.status(401).json( {message: 'Mauvaise combinison d\'email et mot de passe.' })
		}
	)
	dbCtrl.dbClose(db)
}
