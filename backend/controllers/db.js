const sqlite3 = require('sqlite3').verbose()
const path = require('path')
const bcrypt = require('bcrypt')

const dbPath = path.join('app.db')

module.exports.dbConnect = () => {
	let db = new sqlite3.Database(dbPath, err => {
		if (err) {
			console.error(err.message ? err.message : err)
			throw new Error("Error while connecting to the database")
		}
	})
	return (db)
}

module.exports.dbClose = db => {
	db.close (err => {
		if (err) {
			console.error(err.message ? err.message : err)
			throw new Error("Error while closing the database connexion")
		}
	})
}

const createUserTable = db => {
	db.serialize (() => {
		db.run('CREATE TABLE users(userId INTEGER NOT NULL PRIMARY KEY, email VARCHAR(255) NOT NULL UNIQUE, pseudo VARCHAR(255) NOT NULL UNIQUE, password VARCHAR(255), firstName VARCHAR(255), lastName VARCHAR(255), birthDate DATE, country VARCHAR(255), city VARCHAR(255), job VARCHAR(255), hobbies VARCHAR(255), imageUrl VARCHAR(255), admin INTEGER DEFAULT 0)')
		, err => { if (err) console.error(err.message ? err.message : err) }
		bcrypt.hash('modo', 10)
		.then (hash => {
			db.run(
				`INSERT INTO users (email, pseudo, password, admin) VALUES ('modo@modo', 'modo', '${hash}', 1)`
				, []
				, err => {
					if (err)
						throw err
				}
			)
		})
		.catch (err => {
			this.dbClose(db)
			throw err
		})
		db.run('CREATE TABLE threads(id INTEGER NOT NULL PRIMARY KEY, userId INTEGER, username VARCHAR(255), title VARCHAR(255), text TEXT, FOREIGN KEY (username)  REFERENCES users(pseudo) ON UPDATE CASCADE, FOREIGN KEY (userId)  REFERENCES users(userId) ON UPDATE CASCADE)'
		, err => { if (err) console.error(err.message ? err.message : err) })
		.run('CREATE TABLE posts(id INTEGER NOT NULL PRIMARY KEY, userId INTEGER, username VARCHAR(255), title VARCHAR(255), threads_id INTEGER, text TEXT, FOREIGN KEY (username)  REFERENCES users(pseudo) ON UPDATE CASCADE, FOREIGN KEY (userId)  REFERENCES users(userId) ON UPDATE CASCADE ON DELETE CASCADE, FOREIGN KEY (threads_id) REFERENCES threads(id) ON DELETE CASCADE ON UPDATE CASCADE)'
		, err => {
			if (err)
				console.error(err.message ? err.message : err) 
		})
	})
}

const ifTableNoExist = (db, table, toDo, ...args) => {
	db.get(
		"SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name=?",
		[table],
		(err, row) => {
			if (err)
				return console.error(err.message ? err.message : err)
			if (!row['COUNT(*)'])
				toDo(...args)
			else
				this.dbClose(db)
		}
	)
}

module.exports.dbsCreate = () => {
	let db = this.dbConnect()
	ifTableNoExist(db, 'users', createUserTable, db)
}

