const dbCtrl = require('./db')
const fs = require('fs')
const path = require('path')    
const bcrypt = require('bcrypt')


exports.getOneProfile = (req, res, next) => {
	let db ; try { db = dbCtrl.dbConnect() } catch { res.status(500).json({}) }
	db.get(
		"SELECT * FROM users WHERE userId=?",
		[req.params.id],
		(err, row) => {
			if (err) {
				res.status(500).json({})
			}
			else if (row) {
				res.status(200).json(row)
			}
			else
				res.status(401).json( { error: 'userId invalid.' })
		}
	)
	dbCtrl.dbClose(db)
}

exports.getProfile = (userId) => {
	return new Promise ((resolve, reject) => {
		let db ; try { db = dbCtrl.dbConnect() } catch { return null }
		db.get(
			"SELECT * FROM users WHERE userId=?",
			[userId],
			(err, row) => {
				dbCtrl.dbClose(db)
				if (row)
					resolve(row)
				else 
					reject(err)
			}
		)
	})
}

const deleteImage = sauce => {
	// compatible with unix paths and windows paths
	if(!sauce.imageUrl)
		return
	const url = sauce.imageUrl.split(path.sep)
	const filename = url.pop()
	const dir = url.join('/').split('/').slice(-1)[0]
	fs.unlinkSync(dir + path.sep + filename)
}

const buildSql = (data, userId, dataOld) => {
	return new Promise(resolve => {
		if (!data.password || dataOld.admin) {
			delete data.password
			resolve()
		}
		else {
			bcrypt.hash(data.password, 10)
			.then (hash => {
				data.password = hash
				resolve()
			})
		}
	})
	.then (() => {
		if (!data.email || dataOld.admin)
			delete data.email
		if (!data.pseudo || dataOld.admin)
			delete data.pseudo
		let sql = 'UPDATE users SET '
		let x = Object.keys(data).length
		Object.keys(data).forEach(key => {
			sql += `${key} = '${data[key]}'`
			if (--x)
				sql += ', '
		})
		sql += ` WHERE userId = ${userId}`
		return sql
	})
	.catch (err => console.error(err.message ? err.message : err))
}

exports.modifyProfile = async (req, res, next) => {
	let profileMod = req.body
	let profile = await this.getProfile(req.params.id)
	if (req.file) {
		profileMod.imageUrl = `${req.protocol}://${req.get('host')}/${req.file.path}`
		deleteImage(profile)
	}
	else
		delete profileMod.image
	sql = await buildSql(profileMod, req.params.id, profile)
	let db ; try { db = dbCtrl.dbConnect() } catch(err) { return res.status(500).json({}) }
	db.run(sql, [] , err => {
		if (err) {
			console.log(err.message)
			const constraintError = err.message.split(':').slice(-1)[0].trim()
			if (constraintError === "users.pseudo")
				res.status(400).json({ message: 'Le pseudo est déjà utilisé.' })
			else if (constraintError === "users.email")
				res.status(400).json({ message: 'L\'email est déjà utilisé.' })
			else
				res.status(500).json({ error: 'Erreur pendant la modification du profile.' })
		}
		else
			res.status(200).json({ message: 'Profile modifié.' })
	})
	dbCtrl.dbClose(db)
}

exports.deleteProfile = async (req, res, next) => {
	let profile = await this.getProfile(req.params.id)
	if (profile.imageUrl)
		deleteImage(profile)
	if (profile.admin)
		return res.status(500).json({ error: 'Le compte admin ne peut être supprimé.' })
	let db ; try { db = dbCtrl.dbConnect() } catch(err) { return res.status(500).json({}) }
	db.run(`DELETE FROM users WHERE userId = ${req.params.id}`, [] , err => {
		if (err) { 
			res.status(500).json({ error: 'Erreur pendant la suppression du profile.' })
		}
		else
			res.status(200).json({ message: 'Profile supprimé.' })
	})
	dbCtrl.dbClose(db)
}
