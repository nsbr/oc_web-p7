const jwt = require('jsonwebtoken')
const fs = require('fs')
const dbCtrl = require('../controllers/db')

module.exports = async (req, res, next) => {
	let db ; try { db = dbCtrl.dbConnect() } catch { res.status(500).json({}) }
	try {
		const token = req.headers.authorization.split(' ')[1]
		const decodedToken = jwt.verify(token, 'WryubBedMyshoaDraBlabcod9drukJov')
		userId = decodedToken.userId
		if (req.body.userId && req.body.userId === userId)
			return next()
		let user = await getUser(userId)
		if (user.admin)
			return next()
		db.all(
			"SELECT * FROM posts WHERE id=?",
			[req.params.id],
			(err, row) => {
				if (err || !row.length)
					res.status(500).json({})
				else if (row[0].userId !== userId)
					res.status(401).json( {error: 'Bad Token' })
				if (!res.headersSent)
						next()
			}
		)
	}
	catch (err) {
		if (req.file)
			fs.unlinkSync(req.file.path)
		dbCtrl.dbClose(db)
		if (!res.headersSent)
			res.status(403).send({error: "403: unauthorized request"})
	}
}

const getUser = (userId) => {
	return new Promise ((resolve, reject) => {
		let db ; try { db = dbCtrl.dbConnect() } catch { return null }
		db.get(
			"SELECT * FROM users WHERE userId=?",
			[userId],
			(err, row) => {
				dbCtrl.dbClose(db)
				if (row)
					resolve(row)
				else 
					reject(err)
			}
		)
	})
}
