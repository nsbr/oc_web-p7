const jwt = require('jsonwebtoken')
const fs = require('fs')
const dbCtrl = require('../controllers/db')

module.exports = (req, res, next) => {
	let db ; try { db = dbCtrl.dbConnect() } catch { res.status(500).json({}) }
	try {
		const token = req.headers.authorization.split(' ')[1]
		const decodedToken = jwt.verify(token, 'WryubBedMyshoaDraBlabcod9drukJov')
		userId = decodedToken.userId
		db.all(
			"SELECT * FROM users WHERE userId=?",
			[req.params.id],
			(err, row) => {
				if (err) {
					console.log(err)
					res.status(500).json({})
				}
				else if (row.length) {
				}
				else {
					res.status(401).json( {error: 'userId Invalid.' })
				}
				if (!res.headersSent)
					next()
			}
		)
	}
	catch (err) {
		if (req.file)
			fs.unlinkSync(req.file.path)
		dbCtrl.dbClose(db)
		if (!res.headersSent)
			res.status(403).send({error: "403: unauthorized request"})
	}
}
