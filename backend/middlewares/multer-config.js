const multer = require('multer')
const fs = require('fs')
const crypto = require('crypto')

const MIME_TYPES = {
	'image/jpg': 'jpg',
	'image/jpeg': 'jpg',
	'image/png': 'png'
}

const storage = multer.diskStorage({
	destination: (req, file, callback) => {
		if (!fs.existsSync('images'))
			fs.mkdirSync('images')
		callback(null, 'images')
	},
	filename: (req, file, callback) => {
		const name = file.originalname.split(' ').join('_')
		const extension = MIME_TYPES[file.mimetype]
		callback(null, Date.now() + String(crypto.randomInt(0,999999)).padStart(6, 0) + '.' + extension)
	}
})

const fileFilter = (req, file, callback) => {
	if (file.mimetype !== 'image/jpg' && file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/png') 
		callback(new Error('Only jpg and png files can be uploaded'))
	else
		callback(null, true)
}

module.exports = multer({storage: storage, fileFilter: fileFilter}).single('image')
