const express = require('express')

const threadsCtrl = require('../controllers/threads')
const auth = require('../middlewares/threads-auth')

const router = express.Router()

router.get('/', threadsCtrl.getAllThreads)
router.post('/', auth, threadsCtrl.createThread)
router.delete('/:id', auth, threadsCtrl.deleteThread)

module.exports = router
