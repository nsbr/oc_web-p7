const express = require('express')

const profileCtrl = require('../controllers/profile')

const auth = require('../middlewares/profile-auth')
const multer = require('../middlewares/multer-config')

const router = express.Router()

router.get('/:id', auth, profileCtrl.getOneProfile)
router.put('/:id', multer, auth, profileCtrl.modifyProfile)
router.delete('/:id', auth, profileCtrl.deleteProfile)

module.exports = router
