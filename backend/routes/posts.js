const express = require('express')

const postsCtrl = require('../controllers/posts')
const auth = require('../middlewares/posts-auth')

const router = express.Router()

router.get('/:id', postsCtrl.getAllPosts)
router.post('/:id', auth, postsCtrl.createPost)
router.delete('/:id', auth, postsCtrl.deletePost)


module.exports = router
