const express = require('express')

const authRoutes = require('./routes/auth')
const profileRoutes = require('./routes/profile')
const threadsRoutes = require('./routes/threads')
const postsRoutes = require('./routes/posts')

const errorHandler = require('./middlewares/error-handler')
const dbCtrl = require('./controllers/db')

const app = express()

// set HTTP Headers
app.use((req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-Width, Content, Accept, Content-Type, Authorization')
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTION')
	next()
})

// create database id necessary
dbCtrl.dbsCreate()


// parse request body
app.use(express.json())

// link images route to server images folder
app.use('/images', express.static('images'))

app.use('/api/auth', authRoutes)
app.use('/api/profile', profileRoutes)
app.use('/api/profile', profileRoutes)
app.use('/api/threads', threadsRoutes)
app.use('/api/posts', postsRoutes)
app.use(errorHandler)

module.exports = app
