import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/signup',
    name: 'Signup',
    component: () => import('../views/Signup.vue')
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import('../views/Profile.vue')
  },
  {
    path: '/modify-profile',
    name: 'ModifyProfile',
    component: () => import('../views/ModifyProfile.vue')
  },
  {
    path: '/add-thread',
    name: 'AddThread',
    component: () => import('../views/AddThread.vue')
  },
  {
    path: '/thread/:id',
    name: 'Thread',
    component: () => import('../views/Thread.vue')
  },
  {
    path: '/add-post',
    name: 'AddPost',
    component: () => import('../views/AddPost.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
