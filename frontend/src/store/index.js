import { createStore } from 'vuex'
import router from '@/router'

let apiUrl = process.env.VUE_APP_API_URL

export default createStore({
	state: {
		logged: null,
		threads: null,
		posts: {}
	},
	mutations: {
		LOGIN(state, login) {
			if (typeof login === 'string')
				state.logged = JSON.parse(login)
			else
				state.logged = login
			
		},
		LOGOUT(state) {
			state.logged = null
		},
		THREADS(state, threads) {
			state.threads = threads
		},
		POSTS(state, posts) {
			state.posts.msgs = posts
		}
	},
	actions: {
		login ({ commit }, login) {
			commit('LOGIN', login)
			if (typeof login === 'string')
				localStorage.setItem('login', login)
			else
				localStorage.setItem('login', JSON.stringify(login))
		},
		logout ({ commit }) {
			commit('LOGOUT')
			localStorage.setItem('login', null)

		},
		loginUpdate({ commit }) {
			let login = localStorage.getItem('login')
			if (!login)
				commit('LOGOUT')
			else
				commit('LOGIN', login)
		},
		getUserData ({ state }) {
			return new Promise ((resolve, reject) => {
				let url = apiUrl + '/profile/' + state.logged.userId
				fetch(url, {
					method: 'GET',
					headers: { 
						'Authorization': 'Bearer ' + state.logged.token
					},
				})
				.then (response => {
					if (response.ok)
						return(response.json())
						else
					throw (`Error ${response.status} while trying to fetch ${url}`)
				})
				.then (data => {
					if (data && data.error)
						console.error(data.error)
					if (data && data.message) {
						console.log(data.message)
					}
					resolve(data)
				})
				.catch (err => reject(err))
			})
		},
		threadsUpdate({ commit }) {
			return new Promise ((resolve, reject) => {
				let url = apiUrl + '/threads/'
				fetch(url, {
					method: 'GET',
				})
				.then (response => {
					if (response.ok)
						return(response.json())
						else
					throw (`Error ${response.status} while trying to fetch ${url}`)
				})
				.then (data => {
					if (data && data.error)
						console.error(data.error)
					if (data && data.message) {
						console.log(data.message)
					}
					commit ('THREADS', data)
					resolve(data)
				})
				.catch (err => reject(err))
			})
		},
		postsUpdate({ commit }, id) {
			return new Promise ((resolve, reject) => {
				let url = apiUrl + '/posts/' + id
				fetch(url, {
					method: 'GET',
				})
				.then (response => {
					if (response.ok)
						return(response.json())
						else
					throw (`Error ${response.status} while trying to fetch ${url}`)
				})
				.then (data => {
					if (data && data.error)
						console.error(data.error)
					if (data && data.message) {
						console.log(data.message)
					}
					commit ('POSTS', data)
					resolve(data)
				})
				.catch (err => reject(err))
			})
		},
		deleteThread({ state }, id) {
			const url = apiUrl + '/threads/' + id
			fetch(url, {
				method: 'DELETE',
				headers: { 
					'Accept': 'application/json', 
					'Authorization': 'Bearer ' + state.logged.token
				},
			})
			.then (response => response.json().then (data => {
					if (data.error)
						console.error(data.error)
					if (data.message) {
						console.log(data.message)
					}
					if (response.ok)
						router.go()
				})
			)
			.catch (err => console.error(err.message ? err.message : err))
		},
		deletePost({ state }, id) {
			const url = apiUrl + '/posts/' + id
			fetch(url, {
				method: 'DELETE',
				headers: { 
					'Accept': 'application/json', 
					'Authorization': 'Bearer ' + state.logged.token
				},
			})
			.then (response => response.json().then (data => {
					if (data.error)
						console.error(data.error)
					if (data.message) {
						console.log(data.message)
					}
					if (response.ok)
						router.go()
				})
			)
			.catch (err => console.error(err.message ? err.message : err))
		}
	}
})
